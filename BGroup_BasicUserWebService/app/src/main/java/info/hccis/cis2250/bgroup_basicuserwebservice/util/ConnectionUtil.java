package info.hccis.cis2250.bgroup_basicuserwebservice.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.ExecutionException;

/**
 * Created by bjmaclean on 1/24/2017.
 */

public class ConnectionUtil {

    /*
 * Checks if internet is connected.
 *
 * http://developer.android.com/training/monitoring-device-state/connectivity-monitoring.html
 */


    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            Connection newConnection = new Connection("http://hccis.info:8080/cisadmin/rest/useraccess/bjmac_codes");
            newConnection.execute();
            try {
                String results = newConnection.get();
                if (!results.isEmpty() && results.equalsIgnoreCase("0")) {
                    isConnected = true;
                } else {
                    isConnected = false;
                }
            } catch (InterruptedException | ExecutionException e) {
                isConnected = false;
            }
        }
        return isConnected;
    }

    public static String getUsers(Context context, String userId) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        String results = "";
        if (isConnected) {
            Connection newConnection = new Connection("http://hccis.info:8080/courtbooking/rest/UserService/user");
//            Connection newConnection = new Connection("http://hccis.info:8080/golftournament/api/tournaments/players/2");

            newConnection.execute();
            try {
                 results = newConnection.get();




            } catch (InterruptedException | ExecutionException e) {
                isConnected = false;
            }
        }
        return results;
    }


}
