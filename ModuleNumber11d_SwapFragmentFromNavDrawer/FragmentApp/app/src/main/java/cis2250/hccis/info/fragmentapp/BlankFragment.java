package cis2250.hccis.info.fragmentapp;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cis2250.hccis.info.fragmentapp.util.ConnectionUtil;
import cis2250.hccis.info.fragmentapp.util.User;
import cis2250.hccis.info.fragmentapp.util.UserAdapter;
import cis2250.hccis.info.fragmentapp.util.UserDatabase;


/**
 */
public class BlankFragment extends Fragment {


    private List<User> userList = new ArrayList<>();
    private RecyclerView recyclerView;
    private UserAdapter mAdapter;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public BlankFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, String param2) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        String jsonReturned = ConnectionUtil.getUsers(view.getContext(), "");
        Toast.makeText(view.getContext(), jsonReturned, Toast.LENGTH_SHORT).show();

        //**************************************************************
        //from: http://www.androidhive.info/2016/01/android-working-with-recycler-view/
        //**************************************************************

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_on_fragment);
        mAdapter = new UserAdapter(userList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        String userJson = ConnectionUtil.getUsers(view.getContext(), "");
        Toast.makeText(view.getContext(), userJson, Toast.LENGTH_LONG).show();

        //*************************************************
        //Next want to take the json and convert it to some Tournament objects.
        //*************************************************

        Gson gson = new Gson();
        User[] userArray = gson.fromJson(userJson, User[].class);

        //***********************************************************************************
        //Now I have all of the users in an array.  I want to add each of these to the
        //Room database.
        //************************************************************************************

        final UserDatabase db = Room.databaseBuilder(view.getContext(), UserDatabase.class, "Production")
                .allowMainThreadQueries()
                .build();


        //********************************************************************************
        // Taking the users which were returned from the web service (array) and loading
        // them into the Room database.
        //********************************************************************************

        for (User current : userArray) {
            try {

                db.userDao().insertAll(current);
            } catch (SQLiteConstraintException e) {
                //****************************************************************************
                //If the primary key already existed in the db, there would be an exception.
                //in this case update the row instead of insert.
                //****************************************************************************
                try {
                    db.userDao().updateAll(current);
                } catch (Exception e2) {
                    System.out.println("Exception updating user");
                }
            }
        }

        //Load the ArrayList from the database.
        ArrayList<User> userListTemp = (ArrayList<User>) db.userDao().getAllUsers();
        Log.d("bjtest", "user list size=" + userList.size());

        //***********************************************************************************
        // Note that the userListTemp is loaded with all of the users.  However this
        // is not enough to make the list show up on the recycler.  They do show up if
        // they are added to the userList like below...
        //***********************************************************************************

        for (User user : userListTemp) {
            Log.d("Added user", "" + user);
            userList.add(user);
        }

        mAdapter.notifyDataSetChanged();


        return view;
    }
}
