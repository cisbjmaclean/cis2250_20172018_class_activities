package cis2250.hccis.info.fragmentapp.util;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cis2250.hccis.info.fragmentapp.R;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private List<User> userList;



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView firstName,lastName,  username, userId;

        public MyViewHolder(View view) {
            super(view);
            firstName = (TextView) view.findViewById(R.id.firstName);
            lastName = (TextView) view.findViewById(R.id.lastName);
            username = (TextView) view.findViewById(R.id.username);
            userId = (TextView) view.findViewById(R.id.userId);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }


    public UserAdapter(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        User user = userList.get(position);
        holder.lastName.setText(user.getLastName());
        holder.firstName.setText(user.getFirstName());
        holder.username.setText(user.getUsername());
        Log.d("bjtest", "user id to be set="+user.getUserId());
        holder.userId.setText(String.valueOf(user.getUserId()));

        //************************************************************************
        //Storing the tournament id in a final attribute.  It can then be used
        //in the onClickListener to know which tournament is clicked
        //http://stackoverflow.com/questions/39074272/respond-to-buttons-click-in-recyclerview
        //************************************************************************

        final int userId = user.getUserId();
        holder.firstName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
            }
        });
        holder.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
            }
        });
        holder.userId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "clicked "+userId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
