package cis2250.hccis.info.fragmentapp.receiver;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.FragmentUsers;
import cis2250.hccis.info.fragmentapp.MainActivity;
import cis2250.hccis.info.fragmentapp.R;
import cis2250.hccis.info.fragmentapp.RestartActivity;
import cis2250.hccis.info.fragmentapp.SplashActivity;

/**
 * Created by bjmaclean on 3/5/2018.
 */

public class MyBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "bjm-MyBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        //****************************************************************
        //Build information to be displayed to the user
        //****************************************************************

        StringBuilder sb = new StringBuilder();
        sb.append("Action: " + intent.getAction() + "\n");
        sb.append("URI: " + intent.toUri(Intent.URI_INTENT_SCHEME).toString() + "\n");
        String log = sb.toString();
        Log.d(TAG, log);
        Toast.makeText(context, log, Toast.LENGTH_LONG).show();

        //*************************************************************************
        // If the screen has just come on then send the user to a restart activity
        //*************************************************************************

        if(intent.getAction().equals(Intent.ACTION_SCREEN_ON))
        {
            Log.d("BJM", "on clicked, reload the bookings");
//            Intent intentMain = new Intent(context, RestartActivity.class);
//            intentMain.putExtra("fromPowerOn", true);
//            context.startActivity(intentMain);


        }
    }
}

