package cis2250.hccis.info.fragmentapp.util;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 */
@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    List<User> getAllUsers();

    @Query("DELETE FROM user")
    void deleteAllUsers();


    @Insert
    void insertAll(User... users);

    @Update
    void updateAll(User... users);

}
