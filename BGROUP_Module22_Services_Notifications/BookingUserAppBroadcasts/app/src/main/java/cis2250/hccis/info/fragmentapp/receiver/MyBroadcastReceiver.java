package cis2250.hccis.info.fragmentapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import cis2250.hccis.info.fragmentapp.service.UserService;

/**
 * Created by bjmaclean on 3/5/2018.
 */

public class MyBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "bjm-MyBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        //****************************************************************
        //Build information to be displayed to the user
        //****************************************************************

        StringBuilder sb = new StringBuilder();
        sb.append("Action: " + intent.getAction() + "\n");
        sb.append("URI: " + intent.toUri(Intent.URI_INTENT_SCHEME).toString() + "\n");
        String log = sb.toString();
        Log.d(TAG, log);
        Toast.makeText(context, log, Toast.LENGTH_LONG).show();

        //*************************************************************************
        // If the screen has just come on or the boot is completed then start the
        // user service.  This user service will load the uses from the web service.
        //*************************************************************************

        if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)
                || intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.d("BJM", "screen on / boot completed");
            Intent startServiceIntent = new Intent(context, UserService.class);
            context.startService(startServiceIntent);
        }
    }
}

