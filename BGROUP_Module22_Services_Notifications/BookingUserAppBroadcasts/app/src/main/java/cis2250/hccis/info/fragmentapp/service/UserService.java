package cis2250.hccis.info.fragmentapp.service;

/**
 * This service class will load the users from the web service into the Room database.
 *
 * Created by bjmaclean on 3/19/2018.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

import cis2250.hccis.info.fragmentapp.R;
import cis2250.hccis.info.fragmentapp.SplashActivity;
import cis2250.hccis.info.fragmentapp.util.ConnectionUtil;
import cis2250.hccis.info.fragmentapp.util.User;
import cis2250.hccis.info.fragmentapp.util.UserDatabase;

public class UserService extends Service {
    private final IBinder mBinder = new MyBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("bjm", "onStartCommand for MyService about to load users from service.");


        UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users.db")
                .allowMainThreadQueries()
                .build();

        try {


            String userJson = ConnectionUtil.getUsers(getApplicationContext(), "");
            //Toast.makeText(view.getContext(), userJson, Toast.LENGTH_LONG).show();

            //*************************************************
            //Next want to take the json and convert it to some Tournament objects.
            //*************************************************

            Gson gson = new Gson();
            User[] userArray = gson.fromJson(userJson, User[].class);

            //***********************************************************************************
            //Now I have all of the users in an array.  I want to add each of these to the
            //Room database.
            //************************************************************************************

            db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "users.db")
                    .allowMainThreadQueries()
                    .build();

            //********************************************************************************
            // Taking the users which were returned from the web service (array) and loading
            // them into the Room database.
            //********************************************************************************

            for (User current : userArray) {
                Log.d("bjm", "loading to db: "+current.toString());
                try {

                    db.userDao().insertAll(current);
                } catch (SQLiteConstraintException e) {
                    //****************************************************************************
                    //If the primary key already existed in the db, there would be an exception.
                    //in this case update the row instead of insert.
                    //****************************************************************************
                    try {
                        db.userDao().updateAll(current);
                    } catch (Exception e2) {
                        System.out.println("Exception updating user");
                    }
                }
            }
            createNotification();

        }catch(Exception e){
            Log.e("bjtest-Error", "Could not connect to the web service" + e.getMessage());
        }

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("bjm", "onBind for Service");
        return mBinder;
    }

    public class MyBinder extends Binder {
        UserService getService() {
            return UserService.this;
        }
    }

    /**
     * creating the notification
     * @since 20180319
     * @author Christal / BJ
     */

    private void createNotification(){
        //If the notification is chosen we will send them to the application.
        Intent intent = new Intent(this,SplashActivity.class);

        //******************************************************************************************************
        //Pending intent is a token that you give to another application (ie. notification manager, alarm manager,
        // or other third-party application) which allows these other application to use the permissions of your
        //application to execute predefined code.
        //*********************************************************************************************************
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int)System.currentTimeMillis(), intent, 0);

        Notification notification = new Notification.Builder(this)
                //setting content and icon of notification
                .setContentTitle("Users loaded")
                .setContentText("The users for court booking app have been loaded, click to go there.")
                .setSmallIcon(R.drawable.logo)//find icon and put in your drawable folder
                .setContentIntent(pendingIntent)
                .build(); // when you get here the .build() becomes red if your gradle.build file min is below 16
        //go to the file and put the min to 16

        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        //this will cancel the notification once it is clicked
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);

    }


}