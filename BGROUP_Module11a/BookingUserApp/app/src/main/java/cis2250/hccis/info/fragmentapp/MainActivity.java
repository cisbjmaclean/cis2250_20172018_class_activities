package cis2250.hccis.info.fragmentapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BlankFragment.OnFragmentInteractionListener{

    private static FragmentManager fm;
    FragmentTransaction ft;

    public static FragmentManager getCurrentFragmentManager(){
        return fm;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //*****************************************************
        //Video 5 reviews:  Floating action bar
        //*****************************************************

        fm = this.getFragmentManager();
        ft = fm.beginTransaction();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);




        //******************************************************************
        // When the fab is clicked, change the fragment on the activity.
        //******************************************************************

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = MainActivity.getCurrentFragmentManager().beginTransaction();
                ft.replace(R.id.fragment, new FragmentUsers());
                ft.commit();

                Snackbar.make(view, "Loaded Users Fragment", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //******************************************************************************************
        // Set the initial fragment on the main activity layout
        //******************************************************************************************

        if (savedInstanceState == null) {
            ft.replace(R.id.fragment, new BlankFragment());
            ft.commit();
        }

    }

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     * @since 20180116
     * @author BJM/CIS2250
     * @param activity
     * @param title
     * @param message
     */

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //*****************************************************
        // Video 5 reviews. more menu
        //*****************************************************
        if (id == R.id.action_settings) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************
            Toast.makeText(getApplicationContext(),
                    "Hi, settings was chosen", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //nav camera
        } else if (id == R.id.nav_gallery) {
            //**************************************************************************************
            // Video 5 reviews: Nav bar items.
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************

            Toast.makeText(getApplicationContext(),
                    "hi, nav gallery was chosen.", Toast.LENGTH_SHORT).show();
            // Handle the camera action


        } else if (id == R.id.nav_share) {

            Uri webpage = Uri.parse("http://hccis.info:8080/courtbooking");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
            startActivity(webIntent);
            finish();

        } else if (id == R.id.nav_send) {

            addEvent();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void addEvent() {
        //Define start time in milliseconds
        long startMillis = 0;
        //Define end time in milliseconds
        long endMillis = 0;




        //Create an instance for setting the beginning time of the calendar
        Calendar beginTime = Calendar.getInstance();
        //Pass the parameters to set beginning time

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        Toast.makeText(getBaseContext(), "New event inserted "+year+" "+month+" "+day, Toast.LENGTH_LONG)
                .show();

        beginTime.set(year, month, day, 8, 15);
        //Get begin date in milliseconds after calculation
        startMillis = beginTime.getTimeInMillis();
        //Create an instance for setting the end date of the event
        Calendar endTime = Calendar.getInstance();
        //Pass the parameters to set end date of the event
        endTime.set(year, month, day, 9, 15);
        //Get end date in milliseconds after calculation
        endMillis = endTime.getTimeInMillis();
        //Create content resolver object to insert data
        ContentResolver cr = getContentResolver();
        //Create content values object to insert data
        ContentValues values = new ContentValues();
        //Get default time zone
        TimeZone timeZone = TimeZone.getDefault();
        Log.d("info", "The default time zone is  " + timeZone);
        //Insert the data into the calendar
        values.put(CalendarContract.Events.DTSTART, startMillis); //Start date
        values.put(CalendarContract.Events.DTEND, endMillis); //End date
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID()); //Time zone ID
        values.put(CalendarContract.Events.TITLE, "User App Event"); //Title of the event
        values.put(CalendarContract.Events.DESCRIPTION,"Add only one event"); //Description of the event
        values.put(CalendarContract.Events.CALENDAR_ID, 1);//Set the Id of the event
//        values.put(CalendarContract.Events.ALL_DAY, 1);
        values.put(CalendarContract.Events.HAS_ALARM, 1); //Configure alarm

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        // Retrieve ID for new event
        String eventID = uri.getLastPathSegment();
        Log.d("info","The event was added with id "+eventID+"in calendar ");
        Toast.makeText(getBaseContext(), "New event inserted The event was added with id "+eventID+"in calendar ", Toast.LENGTH_LONG)
                .show();


    }




    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
