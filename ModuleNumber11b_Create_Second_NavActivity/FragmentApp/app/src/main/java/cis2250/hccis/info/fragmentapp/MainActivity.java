package cis2250.hccis.info.fragmentapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cis2250.hccis.info.fragmentapp.util.ConnectionUtil;
import cis2250.hccis.info.fragmentapp.util.User;
import cis2250.hccis.info.fragmentapp.util.UserAdapter;
import cis2250.hccis.info.fragmentapp.util.UserDatabase;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentMacLeanBJ.OnFragmentInteractionListener,
                FragmentChristal.OnFragmentInteractionListener{

    private List<User> userList = new ArrayList<>();
    private RecyclerView recyclerView;
    private UserAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //*****************************************************
        //Video 5 reviews:  Floating action bar
        //*****************************************************

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        String jsonReturned = ConnectionUtil.getUsers(getApplicationContext(), "");
        Toast.makeText(getApplicationContext(), jsonReturned, Toast.LENGTH_LONG).show();

        //**************************************************************
        //from: http://www.androidhive.info/2016/01/android-working-with-recycler-view/
        //**************************************************************

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new UserAdapter(userList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        String userJson = ConnectionUtil.getUsers(getApplicationContext(),"");
        Toast.makeText(getApplicationContext(), userJson, Toast.LENGTH_LONG).show();

        //*************************************************
        //Next want to take the json and convert it to some Tournament objects.
        //*************************************************

        Gson gson = new Gson();
        User[] userArray = gson.fromJson(userJson, User[].class);

        //***********************************************************************************
        //Now I have all of the users in an array.  I want to add each of these to the
        //Room database.
        //************************************************************************************

        final UserDatabase db = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "Production")
                .allowMainThreadQueries()
                .build();


        //********************************************************************************
        // Taking the users which were returned from the web service (array) and loading
        // them into the Room database.
        //********************************************************************************

        for(User current: userArray){
            try {

                db.userDao().insertAll(current);
            }catch(SQLiteConstraintException e){
                //****************************************************************************
                //If the primary key already existed in the db, there would be an exception.
                //in this case update the row instead of insert.
                //****************************************************************************
                try {
                    db.userDao().updateAll(current);
                }catch(Exception e2){
                    System.out.println("Exception updating user");
                }
            }
        }

        //Load the ArrayList from the database.
        ArrayList<User> userListTemp = (ArrayList<User>) db.userDao().getAllUsers();
        Log.d("bjtest", "user list size="+userList.size());

        //***********************************************************************************
        // Note that the userListTemp is loaded with all of the users.  However this
        // is not enough to make the list show up on the recycler.  They do show up if
        // they are added to the userList like below...
        //***********************************************************************************

        for(User user: userListTemp){
            Log.d("Added user",""+user);
            userList.add(user);
        }

        mAdapter.notifyDataSetChanged();

    }

    /**
     * This method can be used to show a dialog in your activity.  This will allow the user to
     * enter a positive or negative response.
     * @since 20180116
     * @author BJM/CIS2250
     * @param activity
     * @param title
     * @param message
     */

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-positive", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Not Really", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-negative", Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //*****************************************************
        // Video 5 reviews. more menu
        //*****************************************************
        if (id == R.id.action_settings) {
            //**************************************************************************************
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************
            Toast.makeText(getApplicationContext(),
                    "Hi, settings was chosen", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //nav camera
        } else if (id == R.id.nav_gallery) {
            //**************************************************************************************
            // Video 5 reviews: Nav bar items.
            // make a toast indicating that a nav bar item was chosen
            //**************************************************************************************

            Toast.makeText(getApplicationContext(),
                    "hi, nav gallery was chosen.", Toast.LENGTH_SHORT).show();
            // Handle the camera action


        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String message) {
        Log.d("bjtest", "interaction with fragment happened: "+message);
    }
}
