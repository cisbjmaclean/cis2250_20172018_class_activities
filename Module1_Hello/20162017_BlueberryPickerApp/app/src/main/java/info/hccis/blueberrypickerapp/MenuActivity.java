package info.hccis.blueberrypickerapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Collections;
import java.util.Arrays;

public class MenuActivity extends AppCompatActivity
{

	private Button addPickerBtn = null;
	private TextView details = null;
	private HashMap names = new HashMap();
	private static Integer numPickers = 0;
	private static Integer numBoxes = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		addPickerBtn = (Button) findViewById((R.id.addPickerBtn));
		details = (TextView) findViewById((R.id.textViewDetails));

		try
		{
			Bundle extras = getIntent().getExtras();

			System.out.println("***************************");
			System.out.println(Arrays.asList(names));

			details.setText("Number of Pickers: " + numPickers + "\n"
							+ "Total Boxes: " + numBoxes);
		}
		catch(NullPointerException n)
		{

		}


		addPickerBtn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{
				Log.d("TEST", "The button was clicked");
				Intent intent = new Intent(MenuActivity.this, EnterDetailsActivity.class);
				startActivity(intent);
				finish();
			}
		});

	}

	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save the user's current game state
		savedInstanceState.putSerializable("map", names);


		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}

	public static void increasePickers()
	{
		numPickers++;
	}

	public static void addBoxes(String boxes)
	{
		try {
			Integer boxes1 = Integer.parseInt(boxes);
			numBoxes += boxes1;
		}catch(Exception e){
			Log.d("User Error","Invalid number of boxes");
		}
	}
}
