package info.hccis.blueberrypickerapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import info.hccis.blueberrypickerapp.MenuActivity;
import java.util.HashMap;

public class EnterDetailsActivity extends AppCompatActivity
{

	private Button saveBtn = null;
	private EditText name = null;
	private EditText numberOfBoxes = null;
	private HashMap names = new HashMap();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enter_details);
		saveBtn = (Button) findViewById(R.id.saveBtn);
		name = (EditText) findViewById(R.id.name);
		numberOfBoxes = (EditText) findViewById(R.id.numberOfBoxes);

		saveBtn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{
				Log.d("TEST", "The button was clicked");
				Intent intent2 = new Intent(EnterDetailsActivity.this, MenuActivity.class);

				//****************************************************
				//Do some validation on what the user entered.
				//****************************************************
				String nameEntered = name.getText().toString();
				String numberOfBoxesEntered = numberOfBoxes.getText().toString();

				boolean validName = !nameEntered.isEmpty();
				boolean validNumberOfBoxes = true;
				try{
					int numberBoxesEntered = Integer.parseInt(numberOfBoxesEntered);
				}catch(Exception e){
					validNumberOfBoxes = false;
				}


				boolean errorFound = false;
				if(!(validName && validNumberOfBoxes)){
					errorFound = true;
				}

				if(errorFound){
					String errorMessage = "Error!  ";
					if(!validName){
						errorMessage += System.lineSeparator()+"Name must be provided  ";
					}
					if(!validNumberOfBoxes){
						errorMessage += System.lineSeparator()+"Number of boxes must be provided  ";
					}

					Toast.makeText(EnterDetailsActivity.this, errorMessage,
							Toast.LENGTH_SHORT).show();
				} else {
					//****************************************************
					//If things are ok finish this activity and start the main
					//****************************************************

					intent2.putExtra("name", nameEntered);
					intent2.putExtra("boxes", numberOfBoxesEntered);


					MenuActivity.increasePickers();
					MenuActivity.addBoxes(numberOfBoxes.getText().toString());

					startActivity(intent2);
					finish();
				}
			}
		});

	}
}
